#version 330 core

out vec4 out_color;

in vec2 TexCoords;

uniform sampler2D image;

void main()
{
	out_color = texture(image, TexCoords);
}