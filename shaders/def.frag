#version 330 core

uniform vec3 color;
uniform vec3 lightPos;
uniform vec3 viewPos;

uniform sampler2D diffuse;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

out vec4 out_color;

float BlinnPhong(vec3 L){
	float ambient = 0.1;
	
	vec3 norm = normalize(Normal);
	vec3 lightDir = normalize(L - FragPos);
	float diffuse = max(dot(norm,lightDir), 0.0);

	vec3 viewDir = normalize(viewPos - FragPos);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float specular = pow(max(dot(norm, halfwayDir), 0.0), 32) * 0.5;

	return ambient+diffuse+specular;
}

void main()
{  
	vec3 result = color * BlinnPhong(lightPos);
    out_color = vec4(result,1.0f) * texture(diffuse, TexCoords);
}