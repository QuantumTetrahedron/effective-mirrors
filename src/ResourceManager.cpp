#include "ResourceManager.h"

#include <iostream>
#include <sstream>
#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

std::map<std::string, Shader> ResourceManager::Shaders;
std::map<std::string, Texture> ResourceManager::Textures;

Shader ResourceManager::LoadShader(const char *vShaderFile, const char *fShaderFile, std::string name)
{
    Shaders[name] = Shader(vShaderFile, fShaderFile);
    return Shaders[name];
}

Shader ResourceManager::GetShader(std::string name)
{
    return Shaders.at(name);
}

void ResourceManager::Clear()
{
    for (auto iter : Shaders)
        glDeleteProgram(iter.second.ID);
}

Texture ResourceManager::LoadTexture(const char* file, bool alpha, std::string name){
    Textures[name] = LoadTextureFromFile(file, alpha);
    return Textures[name];
}

Texture ResourceManager::GetTexture(std::string name){
    return Textures.at(name);
}

Texture ResourceManager::LoadTextureFromFile(const char* file, bool alpha){

    Texture texture;
    if (alpha)
    {
        texture.InternalFormat = GL_RGBA;
        texture.ImageFormat = GL_RGBA;
    }

    int width, height, nChannels;
    unsigned char* image = stbi_load(file, &width, &height, &nChannels, 0);

    texture.Generate(width, height, image);

    stbi_image_free(image);
    return texture;
}
