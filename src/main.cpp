#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Camera.h"
#include "Shader.h"
#include "Scene.h"
#include "Renderers.h"
#include "ResourceManager.h"

const GLuint screenWidth = 1024, screenHeight = 768;

bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

Camera camera(screenWidth, screenHeight, glm::vec3(0.0f,1.0f,0.0f));

void UpdateView(const Shader& shader);
void Do_Movement();

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Mirrors", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

    glViewport(0, 0, screenWidth, screenHeight);

    glEnable(GL_DEPTH_TEST);

    Renderers::Init();

    Shader defaultShader = ResourceManager::LoadShader("shaders/def.vert", "shaders/def.frag", "def");
    Shader mirrorShader = ResourceManager::LoadShader("shaders/def.vert", "shaders/mirror.frag", "mirror");
    Shader lampShader = ResourceManager::LoadShader("shaders/lamp.vert", "shaders/lamp.frag", "lamp");

    Shader screenShader = ResourceManager::LoadShader("shaders/screen.vert", "shaders/screen.frag", "screen");

    ResourceManager::LoadTexture("textures/wood.png", false, "wood");
    ResourceManager::LoadTexture("textures/mirror.png", true, "mirror");

    glm::mat4 projection = glm::perspective(camera.Zoom, camera.aspectRatio, 0.1f, 300.0f);

    defaultShader.use();
    defaultShader.setMat4("projection", projection);
    defaultShader.setInt("diffuse", 0);
    defaultShader.setInt("mirrorFramebufferTexture", 1);

    mirrorShader.use();
    mirrorShader.setMat4("projection", projection);
    mirrorShader.setInt("diffuse", 0);
    mirrorShader.setInt("mirrorFramebufferTexture", 1);
    mirrorShader.setInt("screenWidth", screenWidth);
    mirrorShader.setInt("screenHeight", screenHeight);

    screenShader.use();
    screenShader.setInt("image", 0);

    lampShader.use();
    lampShader.setMat4("projection", projection);

    Scene scene(screenWidth, screenHeight);
    scene.Load();

    GameObject fb(glm::vec3(0.0f), glm::vec3(0.0f), glm::vec3(1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("wood"));

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    while(!glfwWindowShouldClose(window))
    {
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        glfwPollEvents();
        Do_Movement();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        UpdateView(defaultShader);
        UpdateView(mirrorShader);
        UpdateView(lampShader);

        Scene::frameDrawCnt = 0;
        scene.Draw(camera);
        //std::cout << Scene::frameDrawCnt << std::endl;

        glfwSwapBuffers(window);
    }

    scene.clear();
    Renderers::Clear();
    ResourceManager::Clear();

    return 0;
}

void UpdateView(const Shader& shader){
    glm::mat4 view = camera.GetViewMatrix();
    shader.use();
    shader.setMat4("view", view);
    shader.setVec3("viewPos", camera.Position);
}

void Do_Movement()
{
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}


