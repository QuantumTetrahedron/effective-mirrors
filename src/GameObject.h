#ifndef GAME_OBJECT
#define GAME_OBJECT
#include "Renderers.h"
#include "Texture.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class GameObject{
public:
    GameObject(glm::vec3 pos, glm::vec3 rot, glm::vec3 s, Renderer* r, Texture tex, glm::vec3 col = glm::vec3(1.0f))
    :position(pos),rotation(rot),scale(s),renderer(r),color(col),texture(tex){}
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;
    glm::vec3 color;
    Texture texture;

    Renderer* renderer;

    virtual void Draw(const Shader& shader) const {
        glm::mat4 model = GetModelMatrix();
        shader.use();

        shader.setMat4("model", model);
        shader.setVec3("color", color);

        glActiveTexture(GL_TEXTURE0);
        texture.Bind();

        renderer->Draw();

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
    virtual void Update() {}

    glm::mat4 GetModelMatrix() const {
        glm::mat4 m;
        m = glm::translate(m, position);
        if(glm::length(rotation)>0.0f)
        {
            m = glm::rotate(m, glm::radians(rotation.y), glm::vec3(0.0f,1.0f,0.0f));
            m = glm::rotate(m, glm::radians(rotation.z), glm::vec3(0.0f,0.0f,1.0f));
            m = glm::rotate(m, glm::radians(rotation.x), glm::vec3(1.0f,0.0f,0.0f));
        }
        m = glm::scale(m, scale);
        return m;
    }
};

#endif // GAME_OBJECT
