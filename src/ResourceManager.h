#ifndef RESOURCE_MANAGER
#define RESOURCE_MANAGER

#include <map>
#include "Shader.h"
#include "Texture.h"

class ResourceManager{
public:
    static std::map<std::string, Shader> Shaders;
    static Shader LoadShader(const char *vShaderFile, const char *fShaderFile, std::string name);
    static Shader GetShader(std::string name);

    static std::map<std::string, Texture> Textures;
    static Texture LoadTexture(const char* file, bool alpha, std::string name);
    static Texture GetTexture(std::string name);

    static void Clear();
private:
    ResourceManager(){}

    static Texture LoadTextureFromFile(const char* file, bool alpha);
};

#endif // RESOURCE_MANAGER
