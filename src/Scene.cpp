#include "Scene.h"
#include "Quad.h"
#include <glm/glm.hpp>

int Scene::frameDrawCnt = 0;

Scene::Scene(unsigned int width, unsigned int height){
    for(int i = 0; i < MAX_DEPTH; ++i)
        framebuffers.push_back(Framebuffer(width,height));
}

void Scene::Draw(const Camera& camera, int mirrorDepth, glm::mat4 mirrorMatrix, int currentMirrorIndex){

    DrawMirrors(camera, mirrorShader, mirrorDepth, mirrorMatrix, currentMirrorIndex);

    DrawObjects(defShader, mirrorMatrix, currentMirrorIndex);
    DrawLamps(lampShader, mirrorMatrix, currentMirrorIndex);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
}

void Scene::DrawMirrors(const Camera& camera, const Shader& shader, int depth, glm::mat4 mirrorMatrix, int currentMirrorIndex){
    shader.use();
    shader.setVec3("lightPos", lamp->position);

    for(int i = 0; i < mirrors.size(); ++i){
        if(i == currentMirrorIndex) continue;

        Mirror *m = &mirrors[i];
        if(depth > 0) {
            glm::vec4 mPos = mirrorMatrix * glm::vec4(m->position.x, m->position.y, m->position.z, 1.0f);
            glm::vec4 mNorm = mirrorMatrix * glm::vec4(m->normal.x, m->normal.y, m->normal.z, 0.0f);

            float dot = glm::dot(glm::normalize(glm::vec3(mNorm)), glm::normalize(camera.Position - glm::vec3(mPos)));

            bool visible = false;

            Quad q1 = m->GetCorners();

            q1 = q1.Multiply(mirrorMatrix);
            q1 = q1.worldToCamera(camera);
            //if (i == 0 && currentMirrorIndex == -1) {
            //    q1.DebugPrint();
            //}
            if(q1.allCornersBehindCamera())
                visible = false;
            else {
                /* todo
                 * Produces wrong result when only some of the mirror's vertices are behind camera.
                */

                /// q1 = q1.clampToGivenZ(camera.near);
                q1 = q1.cameraToScreen(camera);
                //q1 = q1.worldToScreen(camera);

                if (dot > 0) {
                    visible = q1.fitsOnScreen(camera);
                }

                for (const Quad &q : prevMirrors) {
                    visible = visible && q1.Intersects(q);
                    if (!visible) break;
                }
            }
            if (visible) {
                /// Clear framebuffer below
                framebuffers[depth - 1].Bind();
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

                /// Draw mirror stencil
                shader.use();
                shader.setMat4("mirrorMatrix", mirrorMatrix);

                glDepthMask(GL_FALSE);
                m->Draw(shader);
                glDepthMask(GL_TRUE);

                /// Get next matrix
                glm::mat4 M = m->GetMirrorMatrix();

                //if(r < d) mirrorConstraints.push_back(std::make_pair(glm::vec3(mPos) - camera.Position ,constraintAngle));
                prevMirrors.push_back(q1);
                /// Draw mirrored scene on framebuffer below
                Draw(camera, depth - 1, mirrorMatrix * M, i);
                frameDrawCnt++;
                prevMirrors.pop_back();
                //if(r < d) mirrorConstraints.pop_back();

                /// Bind result from below to current framebuffer
                glActiveTexture(GL_TEXTURE1);
                framebuffers[depth - 1].BindTexture();
            }
        }

        /// Draw mirror on current framebuffer
        if(depth == MAX_DEPTH){glBindFramebuffer(GL_FRAMEBUFFER, 0);}
        else{framebuffers[depth].Bind();}

        shader.use();
        shader.setMat4("mirrorMatrix", mirrorMatrix);
        m->Draw(shader);

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void Scene::DrawObjects(const Shader& shader,glm::mat4 mirrorMatrix, int currentMirrorIndex){

    shader.use();
    shader.setVec3("lightPos", lamp->position);
    shader.setMat4("mirrorMatrix", mirrorMatrix);

    Mirror* m;
    if(currentMirrorIndex != -1)
        m = &mirrors[currentMirrorIndex];

    for(GameObject* o : objects){
        if(currentMirrorIndex == -1 || glm::dot(glm::normalize(o->position-m->position), glm::normalize(m->normal)) > 0)
            o->Draw(shader);
    }
}

void Scene::DrawLamps(const Shader& shader, glm::mat4 mirrorMatrix, int currentMirrorIndex){
    shader.use();
    shader.setMat4("mirrorMatrix", mirrorMatrix);
    lamp->Draw(shader);
}

void Scene::Load(){
    defShader = ResourceManager::GetShader("def");
    lampShader = ResourceManager::GetShader("lamp");
    mirrorShader = ResourceManager::GetShader("mirror");

    lamp = new Lamp(glm::vec3 (0.0f, 2.0f,0.0f));
    objects.push_back(new GameObject(glm::vec3(0.0f), glm::vec3(-90.0f, 0.0f, 0.0f), glm::vec3(5.0f), Renderers::Get("plane"), ResourceManager::GetTexture("wood"),glm::vec3(0.7,0.3,0.3)));
    objects.push_back(new GameObject(glm::vec3(1.0f, 0.5f, 0.0f), glm::vec3(0.0f), glm::vec3(1.0f), Renderers::Get("cube"), ResourceManager::GetTexture("wood"), glm::vec3(0.3,0.3,0.7)));
    objects.push_back(new GameObject(glm::vec3(-1.0f, 0.5f, 0.0f), glm::vec3(0.0f), glm::vec3(1.0f), Renderers::Get("cube"), ResourceManager::GetTexture("wood"), glm::vec3(0.3,0.7,0.3)));
    objects.push_back(new GameObject(glm::vec3(0.0f, 0.5f, -4.0f), glm::vec3(0.0f), glm::vec3(1.0f), Renderers::Get("cube"), ResourceManager::GetTexture("wood")));

    mirrors.push_back(Mirror(glm::vec3(0.0f,0.0f,1.0f), glm::vec3(0.0f, 1.5f, -2.5f), glm::vec3(0.0f, 0.0f,0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
    mirrors.push_back(Mirror(glm::vec3(-1.0f,0.0f,0.0f), glm::vec3(2.5f, 1.5f, 0.0f), glm::vec3(0.0f, -90.0f,0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
    mirrors.push_back(Mirror(glm::vec3(0.0f,0.0f,-1.0f), glm::vec3(0.0f, 1.5f, 2.5f), glm::vec3(0.0f, 180.0f, 0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
    mirrors.push_back(Mirror(glm::vec3(1.0f,0.0f,0.0f), glm::vec3(-2.5f, 1.5f, 0.0f), glm::vec3(0.0f, 90.0f, 0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));

    mirrors.push_back(Mirror(glm::vec3(-1.0f,0.0f,1.0f), glm::vec3(1.75f, 1.5f, -1.75f), glm::vec3(0.0f, -45.0f,0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
    mirrors.push_back(Mirror(glm::vec3(1.0f,0.0f,1.0f), glm::vec3(-1.75f, 1.5f, -1.75f), glm::vec3(0.0f, 45.0f,0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
    mirrors.push_back(Mirror(glm::vec3(1.0f,0.0f,-1.0f), glm::vec3(-1.75f, 1.5f, 1.75f), glm::vec3(0.0f, 135.0f, 0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
    mirrors.push_back(Mirror(glm::vec3(-1.0f,0.0f,-1.0f), glm::vec3(1.75f, 1.5f, 1.75f), glm::vec3(0.0f, -135.0f, 0.0f), glm::vec3(2.0f, 3.0f,1.0f), Renderers::Get("plane"), ResourceManager::GetTexture("mirror")));
}

void Scene::clear(){
    delete lamp;
    for(GameObject* o : objects)
        delete o;
    objects.clear();
    mirrors.clear();
}