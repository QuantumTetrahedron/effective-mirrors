#ifndef FRAMEBUFFER
#define FRAMEBUFFER



class Framebuffer{
public:
    Framebuffer(int width, int height);

    void Bind();
    void Unbind();

    void BindTexture();
//private:
    unsigned int id;
    unsigned int texture, depthStencilRBO;
};

#endif // FRAMEBUFFER
