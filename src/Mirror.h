#ifndef MIRROR
#define MIRROR

#include "Quad.h"
#include "GameObject.h"

class Mirror : public GameObject{
public:
    Mirror(glm::vec3 norm, glm::vec3 pos, glm::vec3 rot, glm::vec3 s, Renderer* r, Texture tex, glm::vec3 col = glm::vec3(1.0f))
    : GameObject(pos,rot,s,r,tex,col), normal(glm::normalize(norm)),quad(-0.5f, 0.5f, -0.5f, 0.5f){}

    glm::vec3 normal;

    glm::mat4 GetMirrorMatrix() const {
        float p = position.x * normal.x + position.y * normal.y + position.z * normal.z;

        glm::mat4 mat;

        mat[0][0] = 1-2*normal.x*normal.x;
        mat[0][1] = -2*normal.x*normal.y;
        mat[0][2] = -2*normal.x*normal.z;
        mat[0][3] = 0;
        mat[1][0] = -2*normal.x*normal.y;
        mat[1][1] = 1-2*normal.y*normal.y;
        mat[1][2] = -2*normal.y*normal.z;
        mat[1][3] = 0;
        mat[2][0] = -2*normal.x*normal.z;
        mat[2][1] = -2*normal.y*normal.z;
        mat[2][2] = 1-2*normal.z*normal.z;
        mat[2][3] = 0;
        mat[3][0] = 2*normal.x*p;
        mat[3][1] = 2*normal.y*p;
        mat[3][2] = 2*normal.z*p;
        mat[3][3] = 1;

        return mat;
    }

    Quad GetCorners() const {
        return quad.Multiply(GetModelMatrix());
    }

private:
    Quad quad;
};

#endif // MIRROR
