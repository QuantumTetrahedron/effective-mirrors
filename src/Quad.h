
#ifndef MIRRORS_QUAD_H
#define MIRRORS_QUAD_H

#include <glm/glm.hpp>
#include "Camera.h"

class Quad {
public:
    Quad(float left, float right, float bottom, float top);

    bool Intersects(const Quad& other) const;

    Quad Multiply(const glm::mat4& mat) const;

    void DebugPrint() const ;

    //Quad worldToScreen(const Camera& camera);
    Quad worldToCamera(const Camera& camera);
    Quad cameraToScreen(const Camera& camera);
    bool fitsOnScreen(const Camera& camera);
    bool allCornersBehindCamera() const;
    bool someCornersBehindCamera() const;
private:

    glm::mat4 corners;
};


#endif //MIRRORS_QUAD_H
