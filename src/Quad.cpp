
#include <iostream>
#include <glad/glad.h>
#include <array>
#include "Quad.h"

Quad::Quad(float left, float right, float bottom, float top) {
    corners[0][0] = left;
    corners[0][1] = top;
    corners[0][2] = 0;
    corners[0][3] = 1;
    corners[1][0] = right;
    corners[1][1] = top;
    corners[1][2] = 0;
    corners[1][3] = 1;
    corners[2][0] = left;
    corners[2][1] = bottom;
    corners[2][2] = 0;
    corners[2][3] = 1;
    corners[3][0] = right;
    corners[3][1] = bottom;
    corners[3][2] = 0;
    corners[3][3] = 1;
}

bool Quad::Intersects(const Quad &other) const {
    //if(allCornersBehindCamera() || other.allCornersBehindCamera()) return false;
    //if(someCornersBehindCamera() || other.someCornersBehindCamera()) return true;

    for(int p = 0; p<2; ++p)
    for(int a = 0; a <=3; a+=3)
    {
        for(int b = 1; b <= 2; ++b)
        {
            double tgaInv;
            if(p == 0)
                tgaInv = (corners[a][0] - corners[b][0]) / (corners[b][1] - corners[a][1]);
            else
                tgaInv = (other.corners[a][0] - other.corners[b][0]) / (other.corners[b][1] - other.corners[a][1]);
            double atan = glm::degrees(glm::atan(tgaInv));
            double cos = glm::cos(glm::radians(atan));
            double sin = glm::sin(glm::radians(atan));

            double thisMin = corners[0][0] * cos - corners[0][1] * sin;
            double thisMax = thisMin;
            double otherMin = other.corners[0][0] * cos - other.corners[0][1] * sin;
            double otherMax = otherMin;

            for(int i = 1; i<=3; ++i){
                double t = corners[i][0] * cos - corners[i][1] * sin;
                thisMin = t < thisMin ? t : thisMin;
                thisMax = t > thisMax ? t : thisMax;
                t = other.corners[i][0] * cos - other.corners[i][1] * sin;
                otherMin = t < otherMin ? t : otherMin;
                otherMax = t > otherMax ? t : otherMax;
            }

            //std::cout << "this = " << thisMin << ", " << thisMax << "  -  other = " << otherMin << ", " << otherMax << std::endl;
            if(thisMax < otherMin || thisMin > otherMax){
                return false;
            }
        }
    }
    return true;
}

Quad Quad::Multiply(const glm::mat4 &mat) const {
    Quad ret(*this);
    ret.corners = mat * ret.corners;
    return ret;
}

void Quad::DebugPrint() const {
    std::cout << "A = " << corners[0][0] << "," << corners[0][1] << "," << corners[0][2] << "," << corners[0][3] << std::endl;
    std::cout << "B = " << corners[1][0] << "," << corners[1][1] << "," << corners[1][2] << "," << corners[1][3] << std::endl;
    std::cout << "C = " << corners[2][0] << "," << corners[2][1] << "," << corners[2][2] << "," << corners[2][3] << std::endl;
    std::cout << "D = " << corners[3][0] << "," << corners[3][1] << "," << corners[3][2] << "," << corners[3][3] << std::endl;
}

/*Quad Quad::worldToScreen(const Camera &camera) {
    Quad ret(*this);
    ret.corners = glm::perspective(camera.Zoom, camera.aspectRatio, 0.1f, 300.0f) * camera.GetViewMatrix() * ret.corners;
    for(int i = 0; i<= 3; ++i) {
        for (int j = 0; j <= 3; ++j){
            float sign = ret.corners[i][j] / glm::abs(ret.corners[i][j]);
            ret.corners[i][j] /= glm::abs(ret.corners[i][3]);
            float sign2 = ret.corners[i][j] / glm::abs(ret.corners[i][j]);
            if(sign != sign2) std::cout << "ZJEBANEEEEEE" << std::endl;
        }
    }
    for(int i = 0;i<=3;++i){
        ret.corners[i][0] = glm::round( ( (ret.corners[i][0]+1)/2.0f) * camera.screenWidth );
        ret.corners[i][1] = glm::round( ( (1-ret.corners[i][1])/2.0f) * camera.screenHeight );
    }
    return ret;
}*/

bool Quad::fitsOnScreen(const Camera &camera) {
    //if(allCornersBehindCamera())
    //    return false;
    Quad screenQuad(0,camera.screenWidth, 0, camera.screenHeight);
    return Intersects(screenQuad);
}

bool Quad::allCornersBehindCamera() const {
    bool ret = true;
    for(int i = 0; i <= 3; ++i)
    {
        //ret = ret && corners[i][3] < 0;
        ret = ret && corners[i][2] > 0;
    }
    return ret;
}

bool Quad::someCornersBehindCamera() const {
    bool ret = false;
    for(int i = 0; i <= 3; ++i)
    {
        //ret = ret || corners[i][3] < 0;
        ret = ret || corners[i][2] > 0;
    }
    return ret;
}

Quad Quad::worldToCamera(const Camera &camera) {
    Quad ret(*this);
    ret.corners = camera.GetViewMatrix() * ret.corners;
    return ret;
}

Quad Quad::cameraToScreen(const Camera &camera) {
    Quad ret(*this);
    ret.corners = glm::perspective(camera.Zoom, camera.aspectRatio, 0.1f, 300.0f) * ret.corners;
    for(int i = 0; i<= 3; ++i) {
        for (int j = 0; j <= 3; ++j){
            ret.corners[i][j] /= glm::abs(ret.corners[i][3]);
        }
    }
    for(int i = 0;i<=3;++i){
        ret.corners[i][0] = glm::round( ( (ret.corners[i][0]+1)/2.0f) * camera.screenWidth );
        ret.corners[i][1] = glm::round( ( (1-ret.corners[i][1])/2.0f) * camera.screenHeight );
    }
    return ret;
}
