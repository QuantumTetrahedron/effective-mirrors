#ifndef TEXTURE
#define TEXTURE

class Texture{
public:
    unsigned int ID;
    unsigned int Width, Height;
    unsigned int InternalFormat;
    unsigned int ImageFormat;

    unsigned int WrapS, WrapT;
    unsigned int FilterMin, FilterMax;

    Texture();
    void Generate(unsigned int width, unsigned int height, unsigned char* data);
    void Bind() const;
};

#endif // TEXTURE
