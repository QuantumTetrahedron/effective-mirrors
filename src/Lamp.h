#ifndef LAMP
#define LAMP

class Lamp : public GameObject{
public:
    Lamp(glm::vec3 pos)
    : GameObject(pos, glm::vec3(0.0f),glm::vec3(0.2f), Renderers::Get("cube"), Texture()){}
};

#endif // LAMP
