#ifndef SCENE
#define SCENE

#include "Mirror.h"
#include "Lamp.h"
#include "ResourceManager.h"
#include "Shader.h"

#include "Framebuffer.h"
#include "Camera.h"
#include <vector>

#define MAX_DEPTH 6

class Scene {
public:
    std::vector<GameObject*> objects;
    std::vector<Mirror> mirrors;
    Lamp* lamp;

    static int frameDrawCnt;

    Scene(unsigned int width, unsigned int height);

    void Draw(const Camera& camera, int mirrorDepth = MAX_DEPTH, glm::mat4 mirrorMatrix = glm::mat4(), int currentMirrorIndex = -1);

    void DrawMirrors(const Camera& camera, const Shader& shader, int depth, glm::mat4 mirrorMatrix, int currentMirrorIndex = -1);
    void DrawObjects(const Shader& shader,glm::mat4 mirrorMatrix, int currentMirrorIndex);
    void DrawLamps(const Shader& shader, glm::mat4 mirrorMatrix, int currentMirrorIndex);

    void Load();

    void clear();

    std::vector<Framebuffer> framebuffers;
private:
    Shader defShader, lampShader, mirrorShader;

    std::vector<Quad> prevMirrors;

};

#endif // SCENE
