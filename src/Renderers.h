#ifndef RENDERERS
#define RENDERERS

#include "Shader.h"
#include <map>

class Renderer{
public:
    Renderer(){
        InitRenderData();
    }
    virtual void Draw() const = 0;
protected:
    virtual void InitRenderData(){};
    unsigned int VAO;
};

class CubeRenderer : public Renderer{
public:
    CubeRenderer(){
        InitRenderData();
    }
    void Draw() const override;
protected:
    void InitRenderData() override;
};

class PlaneRenderer : public Renderer{
public:
    PlaneRenderer(){
        InitRenderData();
    }
    void Draw() const override;
protected:
    void InitRenderData() override;
};

class Renderers {
public:
    static void Init();

    static void Clear();

    static Renderer* Get(std::string name);
private:
    Renderers(){}

    static std::map<std::string, Renderer*> renderers;
};

#endif // RENDERERS
